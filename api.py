

from flask import Flask, render_template
from pymongo import MongoClient
import requests

try:
    con = MongoClient()
    db = con['teste']
except Exception as e:
    print(e)
    exit()

app = Flask(__name__) 

@app.route("/")
def index():
    var = list(range(10))
    return render_template("index.html", numeros=var)

@app.route("/cep")
def teste():
    data = requests.get('https://viacep.com.br/ws/03153002/json')
    print(data.json())
    return render_template('index2.html', data=data.json())

if __name__ == "__main__":
    app.run(debug=True)